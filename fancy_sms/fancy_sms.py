from zeep import Client


class StartWithZero(Exception):
    pass


class ExistPhoneNumber(Exception):
    pass


class LimitLengthText(Exception):
    pass


class LengthTextNotNull(Exception):
    pass


class Sms(object):
    def __init__(self):
        self.sms_response_status = {
            -1: "نام کاربري يا کلمه عبور وارد شده اشتباه است",
            -2: "ارسال از طريق وب سرويس براي اين کاربر غیر فعال است",
            -3: "سرويس موقتا غیر فعال است",
            -4: "شماره فرستنده متعلق به اين کاربر نیست",
            -5: "شماره تلفن همراه گیرنده اشتباه است",
            -6: "اعتبار کاربر براي ارسال کافي نیست",
            -7: "آرايه گیرندگان خالي است",
            -8: "تعداد شماره هاي گیرنده موجود در آرايه بیشتر از تعداد مجاز است",
            -9: "شماره فرستنده اشتباه است",
            -10: "آرايه شناسه پیام خالي است",
            -11: "حساب کاربر مسدود است",
            -12: "تلفن همراه کاربر فعال نیست",
            -13: "ايمیل کاربر فعال نیست",
            -14: "شماره اختصاصي گیرنده پیام اشتباه است",
            -15: "تعداد پیامک هاي درخواستي خارج از محدوده مجاز است",
            -16: "تاخیر مجاز بايد يک عدد بین 0 تا 24 باشد",
            -18: "متن پیام اشتباه است",
            -19: "تعداد خانه هاي آرايه متن بايد برابر 1 يا به تعداد خانه هاي آرايه دريافت کننده باشد",
            -20: "تعداد خانه هاي آرايه فرستنده بايد برابر 1 يا به تعداد خانه هاي آرايه دريافت کننده باشد",
            -21: "تعداد خانه هاي آرايه تاخیر مجاز بايد برابر 1 يا به تعداد خانه هاي آرايه دريافت کننده باشد",
            -30: "",
        }

    def send_message(self, phone_numbers, text):
        """sends an SMS using Payam_Resan to number specified with message"""
        wsdl = "https://www.payam-resan.com/ws/v2/ws.asmx?WSDL"
        client = Client(wsdl)
        phone_numbers = phone_numbers
        sms_text_body = str(text).strip()

        if len(sms_text_body) == 0:
            raise LengthTextNotNull("متن پیام نمیتواند خالی باشد")
        if len(sms_text_body.encode("utf-8")) > 140:
            raise LimitLengthText("تعداد کاراکتر مجاز نمیباشد")
        if [phone for phone in phone_numbers if not str(phone).startswith(('9', '09', '0', '+989'))]:
            raise StartWithZero("شماره گیرنده باید با ۰۹ یا ۹ شروع شود")
        if not phone_numbers:
            raise ExistPhoneNumber("شماره تلفن گیرنده ای موجود نمیباشد")
        if len(phone_numbers) > 99:
            raise ExistPhoneNumber("تعداد شماره های گیرنده باید کمتر از ۹۹ باشد")

        request_data = {
            'Username': "09152464454",
            'PassWord': "Iman09358765217",
            "SenderNumber": "500054031",
            "RecipientNumbers": {
                'string': phone_numbers
            },
            "MessageBodie": sms_text_body,
            "Type": 1,
            "AllowedDelay": 5
        }
        sms_response = client.service.SendMessage(**request_data)
        responses = []
        for idx, status_code in enumerate(sms_response):
            if status_code > 0:
                message = "پیام با موفقیت ارسال گردید"
            else:
                message = self.sms_response_status[status_code]
            responses.append({
                "status_code": status_code,
                "message": message,
                "phone": phone_numbers[idx]
            })
        return responses
