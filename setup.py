import re

import setuptools


name = 'fancysms'
owner = 'miladranaei_siadat'

with open('fancy_sms/__init__.py') as f:
    version = re.search(r'([0-9]+(\.dev|\.|)){3}', f.read()).group(0)

setuptools.setup(
    name=name,
    version=version,
    author=owner.capitalize(),
    author_email="m.ranaei@mail.ru",
    description="Sms wrapper for python",
    packages=setuptools.find_packages(),
    install_requires=['zeep'],
    python_requires='>=3.6'
)